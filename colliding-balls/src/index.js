import { Ball } from "./ball";
import { Field } from "./field";

const canvas = document.getElementById("gameCanvas");
const context = canvas.getContext("2d");
let lasttimestamp;

function resetCanvas() {
    context.fillStyle = "#FFFDD0";
    context.fillRect(0,0,canvas.width, canvas.height);
};

const field = new Field(canvas.width, canvas.height);

for(let i = 0; i < 10; i++){
    for(let j = 0; j < 10; j++){
        field.addBall(new Ball(10,50*i+50,50*j+50,Math.floor(Math.random()*200-100),Math.random()*200-100));
    }
}

function drawBalls() {
    context.fillStyle= "red";
    for (let ball of field.balls) {
        context.beginPath();
        context.arc(ball.position.x, ball.position.y, ball.radius, 0, Math.PI*2, true);
        context.closePath();
        context.fill();
    }
};

function mainloop(timestamp) {
    const delta = (timestamp-lasttimestamp)/1000;
    lasttimestamp = timestamp;
    field.update(delta);
    resetCanvas();
    drawBalls();
    requestAnimationFrame(mainloop);
};

lasttimestamp = performance.now();
requestAnimationFrame(mainloop);
