import { Ball } from '../ball.js';

describe('a ball', () => {
    test('has a radius', () => {
        const radius = 2;
        const ball = new Ball(2);
        expect(ball.radius).toBe(radius);
    });

    test('has a position', () => {
        const ball = new Ball(2,1,1);
        expect(ball.position.x).toBe(1);
        expect(ball.position.y).toBe(1);
    });

    test('has a velocity', () => {
        const velocity = {x: 1, y:1};
        const ball = new Ball(10);
        expect(ball.velocity).toEqual({x: 0, y: 0})
        ball.velocity = velocity;
        expect(ball.velocity).toEqual({x: 1, y: 1})
    });
});