import { Ball } from '../ball';
import { resolveCollision, checkCollision } from '../collision'

describe('A collision', () => {
    test(' can be detected', () => {
        const ball1 = new Ball(10, 50, 50);
        const ball2 = new Ball(10, 45, 50);
        const ball3 = new Ball(10, 29, 50);

        expect(checkCollision(ball1, ball2)).toBe(true);
        expect(checkCollision(ball1, ball3)).toBe(false);
        expect(checkCollision(ball2, ball1)).toBe(true);
        expect(checkCollision(ball2, ball3)).toBe(true);
    });
    
    test(' resolves correctly', () => {
        const ball1 = new Ball(10, 50, 52, 10, -10);
        const ball2 = new Ball(10, 50, 48, 10, 10);

        resolveCollision(ball1, ball2);

        expect(ball1.velocity).toEqual({x: 10, y: 10});
        expect(ball2.velocity).toEqual({x: 10, y: -10});
        expect(ball1.position).toEqual({x: 50, y: 60});
        expect(ball2.position).toEqual({x: 50, y: 40});
    });
});