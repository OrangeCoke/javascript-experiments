import { helloWorld } from '../hello_world';

test('helloWorld is equal to "Hello, World!"', () => {
    expect(helloWorld()).toMatch("Hello, World!");
})