import { Ball } from '../ball';
import { Field } from '../field'

describe('a field', () => {
    test('has width and height', () => {
        const field = new Field(960,640);
        expect(field.width).toBe(960);
        expect(field.height).toBe(640);
    });
    test('can contain balls', () => {
        const ball1 = new Ball();
        const ball2 = new Ball();

        const field = new Field(960, 640);

        field.addBall(ball1);
        field.addBall(ball2);

        expect(field.balls[0]).toEqual(ball1);
        expect(field.balls[1]).toEqual(ball2);
    });
    test('throws exception if anything but ball is added', () => {
        const number = 1;
        const field = new Field(960, 640);

        expect(() => { field.addBall(number)}).toThrow(TypeError);
        expect(field.balls).toEqual([]);
    });

    test('updates ball position based on velocity', () => {
        const field = new Field();
        const ball = new Ball(10, 100, 100, 20, 10);
        const delta = 1;
        field.addBall(ball);
        expect(field.balls[0].position).toEqual({x:100,y:100});
        field.update(delta);
        expect(field.balls[0].position).toEqual({x:120, y:110});
    });

    test('bounces ball if they collide with wall', () => {
        const field = new Field(100,100);
        const ball1 = new Ball(10,90,20,10,0);
        const ball2 = new Ball(10,40,90,0,10);
        const ball3 = new Ball(10,90,90,10,10);

        const delta = 1;

        field.addBall(ball1);
        field.addBall(ball2);
        field.addBall(ball3);

        field.update(delta);

        expect(field.balls[0].velocity).toEqual({x: -10,y: 0});
        expect(field.balls[1].velocity).toEqual({x:  0,y: -10});
        expect(field.balls[2].velocity).toEqual({x: -10,y: -10});
    });

    test('pushes ball out of wall if they collide', () => {
        const field = new Field(100,100);
        const ball1 = new Ball(10,95,20,10,0);

        field.collideBallWithWall(ball1);

        expect(ball1.position.x).toBe(90);
        expect(ball1.position.y).toBe(20);

    });
});