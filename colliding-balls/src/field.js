import { Ball } from './ball';
import { checkCollision, resolveCollision } from './collision';

export class Field {

    constructor(width, height) {
        this.width = width;
        this.height = height;

        this.balls = [];
    }

    addBall(ball) {
        if(! (ball instanceof Ball)) { throw TypeError(); }
        this.balls.push(ball);
    }

    collideBallWithWall(ball) {
        let returnValue = false;

        if(ball.position.x > this.width - ball.radius) {
            ball.velocity.x = -ball.velocity.x;
            ball.position.x = this.width-ball.radius;
            returnValue = true;
        }
        if(ball.position.x < ball.radius) {
            ball.velocity.x = -ball.velocity.x;
            ball.position.x = ball.radius;
            returnValue = true;
        }
        if(ball.position.y > this.height - ball.radius) {
            ball.velocity.y = -ball.velocity.y;
            ball.position.y = this.height - ball.radius;
            returnValue = true;
        }
        if(ball.position.y < ball.radius) {
            ball.velocity.y = -ball.velocity.y;
            ball.position.y = ball.radius;
            returnValue = true;
        }
        return returnValue;
    }

    resovleBallCollisions(ball){
        for (let otherBall of this.balls) {
            if(ball !== otherBall){
                if(ball.collides(otherBall)) {
                    ball.resolveCollision(otherBall);
                }
            }
        }
    }

    resolveAllBallCollisions(){
        const collidedList = [];
        for (let ball of this.balls) {
            collidedList.push(false);
        }
        for (let ballindex1 in this.balls) {
            if (this.collideBallWithWall(this.balls[ballindex1])) {
                collidedList[ballindex1] = true;
            }
        }

        for (let ballindex1 in this.balls) {
            if (collidedList[ballindex1]) { continue; }
            for (let ballindex2 in this.balls) {
                if (ballindex1 === ballindex2) { continue; }
                if (collidedList[ballindex2]) { continue; }
                if (checkCollision(this.balls[ballindex1],this.balls[ballindex2])) {
                    resolveCollision(this.balls[ballindex1], this.balls[ballindex2]);
                    collidedList[ballindex1] = true;
                    collidedList[ballindex2] = true;
                    break;
                }
                collidedList[ballindex1] = true;
            }
        }
    }

    update(delta) {
        if (! (typeof delta == 'number' )) { throw TypeError(); }

        for (let ball of this.balls) {
            ball.position.x += ball.velocity.x*delta;
            ball.position.y += ball.velocity.y*delta;
        }
        this.resolveAllBallCollisions();
    }
}