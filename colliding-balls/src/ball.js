export class Ball {
    constructor(radius = 1, xPosition = 0, yPosition = 0, xVelocity = 0, yVelocity = 0){
        this.radius = radius;
        this.position = {
            x: xPosition,
            y: yPosition
        };
        this.velocity = {
            x: xVelocity,
            y: yVelocity
        };
    }

    distanceOfBallCentres(otherBall) {
        return Math.sqrt(Math.pow(this.position.x - otherBall.position.x, 2) + (Math.pow(this.position.y-otherBall.position.y, 2)));
    }

    getVelocityValue() {
        return Math.sqrt(Math.pow(this.velocity.x, 2) + Math.pow(this.velocity.y,2));
    }
}