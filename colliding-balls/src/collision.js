export function resolveCollision(ball1, ball2) {
    const distanceVector = {
        x: ball1.position.x-ball2.position.x,
        y: ball1.position.y-ball2.position.y
    };

    const distanceOfBallCentres = Math.sqrt(Math.pow(distanceVector.x,2)+Math.pow(distanceVector.y,2));

    const normalVector = {
        x: distanceVector.x/distanceOfBallCentres,
        y: distanceVector.y/distanceOfBallCentres
    };

    const centerVector = {
        x: (ball2.position.x+ball1.position.x)/2,
        y: (ball2.position.y+ball1.position.y)/2
    };

    const newDistanceOfBallCentres = ball1.radius+ball2.radius;

    // reassign ball centers
    ball1.position.x = centerVector.x + normalVector.x*newDistanceOfBallCentres/2;
    ball1.position.y = centerVector.y + normalVector.y*newDistanceOfBallCentres/2;
    ball2.position.x = centerVector.x - normalVector.x*newDistanceOfBallCentres/2;
    ball2.position.y = centerVector.y - normalVector.y*newDistanceOfBallCentres/2;

    const normalVelocityOfBall1 = normalVector.x*ball1.velocity.x+normalVector.y*ball1.velocity.y;
    const normalVelocityOfBall2 = normalVector.x*ball2.velocity.x+normalVector.y*ball2.velocity.y;

    const normalVelocityDifference = normalVelocityOfBall2-normalVelocityOfBall1;

    ball1.velocity.x += normalVector.x*(normalVelocityDifference);
    ball1.velocity.y += normalVector.y*(normalVelocityDifference);
    ball2.velocity.x += normalVector.x*(-normalVelocityDifference);
    ball2.velocity.y += normalVector.y*(-normalVelocityDifference);
};

export function checkCollision(ball1, ball2) {
    const sumOfRadii = ball1.radius + ball2.radius;
    const distance = ball1.distanceOfBallCentres(ball2);
    return distance <= sumOfRadii;
};